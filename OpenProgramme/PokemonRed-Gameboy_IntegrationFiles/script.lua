previous_hp = 20

function correct_hp ()
  local hp = data.hp
  if hp < previous_hp then
    local delta = previous_hp - hp
    previous_hp = hp
    return delta
  else
    return 0
  end
end
