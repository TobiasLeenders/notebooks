import numpy as np
import cv2

cap = cv2.VideoCapture('output-0030.avi')
out = cv2.VideoWriter('output-0030_warped.avi', -1, 20.0, (600,400))

while(cap.isOpened()):
  ret, frame = cap.read()
  rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
  resized = cv2.resize(rgb, None, fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR)

#Needs calibration
  #pts1 = np.float32([[8, 228], [455, 114], [226, 761], [542, 591]])
  #pts1 = np.float32([[55, 228], [542, 114], [226, 800], [542, 591]])
  pts1 = np.float32([[55, 228], [542, 114], [226, 800], [610, 600]])
  pts2 = np.float32([[0, 0], [600, 0], [0, 400], [600, 400]])
  matrix = cv2.getPerspectiveTransform(pts1, pts2)

  result = cv2.warpPerspective(resized, matrix, None)
  resized_warped = cv2.resize(result, None, fx=0.2, fy=0.2, interpolation=cv2.INTER_LINEAR)

  out.write(result)

  cv2.imshow('frame',resized)
  cv2.imshow('warped', result)

  if cv2.waitKey(1) & 0xFF == ord('q'):
    break

cap.release()
cv2.destroyAllWindows()
